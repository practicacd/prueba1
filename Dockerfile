FROM debian:buster-slim
##MAINTAINER Luciano gaitan
RUN apt-get update
RUN apt-get install apache2 -y

ENV APACHE_RUN_USER www-data
ENV APACHE_RUN_GROUP www-data
ENV APACHE_PID_FILE /var/run/apache2/apache2.pid
ENV APACHE_RUN_DIR /var/run/apache2
ENV APACHE_LOCK_DIR /var/lock/apache2
ENV APACHE_LOG_DIR /var/log/apache2
RUN mkdir -p $APACHE_RUN_DIR
RUN mkdir -p $APACHE_LOCK_DIR
RUN mkdir -p $APACHE_LOG_DIR

EXPOSE 80
VOLUME var/www/html
COPY web/* var/www/html/
ENTRYPOINT ["/usr/sbin/apache2"]
CMD ["-D","FOREGROUND"]